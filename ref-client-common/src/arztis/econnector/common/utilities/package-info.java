/**
 * Provides classes with utilities for common purposes like SOAP requests, messages and so on.
 */
package arztis.econnector.common.utilities;