/*
 * XML Type:  ELGARegistryStoredQueryIDType
 * Namespace: urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0
 * Java type: arztis.econnector.ihe.generatedClasses.rs.ebxml_regrep.tc.names.oasis.ELGARegistryStoredQueryIDType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.rs.ebxml_regrep.tc.names.oasis.impl;
/**
 * An XML ELGARegistryStoredQueryIDType(@urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.rs.ebxml_regrep.tc.names.oasis.ELGARegistryStoredQueryIDType.
 */
public class ELGARegistryStoredQueryIDTypeImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.rs.ebxml_regrep.tc.names.oasis.ELGARegistryStoredQueryIDType
{
    private static final long serialVersionUID = 1L;
    
    public ELGARegistryStoredQueryIDTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ELGARegistryStoredQueryIDTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
