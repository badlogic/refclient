/*
 * XML Type:  ClaimTypeType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit;


/**
 * An XML ClaimTypeType(@urn:tiani-spirit:ts).
 *
 * This is a complex type.
 */
public interface ClaimTypeType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ClaimTypeType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s7BF52E4086C44A1A966935CB5C33C75C").resolveHandle("claimtypetypeffe0type");
    
    /**
     * Gets the "ClaimValue" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue getClaimValue();
    
    /**
     * True if has "ClaimValue" element
     */
    boolean isSetClaimValue();
    
    /**
     * Sets the "ClaimValue" element
     */
    void setClaimValue(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue claimValue);
    
    /**
     * Appends and returns a new empty "ClaimValue" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue addNewClaimValue();
    
    /**
     * Unsets the "ClaimValue" element
     */
    void unsetClaimValue();
    
    /**
     * Gets the "ServiceRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceRestrictionListType getServiceRestrictionList();
    
    /**
     * True if has "ServiceRestrictionList" element
     */
    boolean isSetServiceRestrictionList();
    
    /**
     * Sets the "ServiceRestrictionList" element
     */
    void setServiceRestrictionList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceRestrictionListType serviceRestrictionList);
    
    /**
     * Appends and returns a new empty "ServiceRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceRestrictionListType addNewServiceRestrictionList();
    
    /**
     * Unsets the "ServiceRestrictionList" element
     */
    void unsetServiceRestrictionList();
    
    /**
     * Gets the "ServiceOptOutList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceOptOutListType getServiceOptOutList();
    
    /**
     * True if has "ServiceOptOutList" element
     */
    boolean isSetServiceOptOutList();
    
    /**
     * Sets the "ServiceOptOutList" element
     */
    void setServiceOptOutList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceOptOutListType serviceOptOutList);
    
    /**
     * Appends and returns a new empty "ServiceOptOutList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceOptOutListType addNewServiceOptOutList();
    
    /**
     * Unsets the "ServiceOptOutList" element
     */
    void unsetServiceOptOutList();
    
    /**
     * Gets the "ServiceDeletionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceDeletionListType getServiceDeletionList();
    
    /**
     * True if has "ServiceDeletionList" element
     */
    boolean isSetServiceDeletionList();
    
    /**
     * Sets the "ServiceDeletionList" element
     */
    void setServiceDeletionList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceDeletionListType serviceDeletionList);
    
    /**
     * Appends and returns a new empty "ServiceDeletionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ServiceDeletionListType addNewServiceDeletionList();
    
    /**
     * Unsets the "ServiceDeletionList" element
     */
    void unsetServiceDeletionList();
    
    /**
     * Gets the "ProviderRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ProviderRestrictionListType getProviderRestrictionList();
    
    /**
     * True if has "ProviderRestrictionList" element
     */
    boolean isSetProviderRestrictionList();
    
    /**
     * Sets the "ProviderRestrictionList" element
     */
    void setProviderRestrictionList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ProviderRestrictionListType providerRestrictionList);
    
    /**
     * Appends and returns a new empty "ProviderRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ProviderRestrictionListType addNewProviderRestrictionList();
    
    /**
     * Unsets the "ProviderRestrictionList" element
     */
    void unsetProviderRestrictionList();
    
    /**
     * Gets the "DocumentRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentRestrictionListType getDocumentRestrictionList();
    
    /**
     * True if has "DocumentRestrictionList" element
     */
    boolean isSetDocumentRestrictionList();
    
    /**
     * Sets the "DocumentRestrictionList" element
     */
    void setDocumentRestrictionList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentRestrictionListType documentRestrictionList);
    
    /**
     * Appends and returns a new empty "DocumentRestrictionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentRestrictionListType addNewDocumentRestrictionList();
    
    /**
     * Unsets the "DocumentRestrictionList" element
     */
    void unsetDocumentRestrictionList();
    
    /**
     * Gets the "DocumentDeletionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentDeletionListType getDocumentDeletionList();
    
    /**
     * True if has "DocumentDeletionList" element
     */
    boolean isSetDocumentDeletionList();
    
    /**
     * Sets the "DocumentDeletionList" element
     */
    void setDocumentDeletionList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentDeletionListType documentDeletionList);
    
    /**
     * Appends and returns a new empty "DocumentDeletionList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.DocumentDeletionListType addNewDocumentDeletionList();
    
    /**
     * Unsets the "DocumentDeletionList" element
     */
    void unsetDocumentDeletionList();
    
    /**
     * Gets the "SignedDocumentList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.SignedDocumentListType getSignedDocumentList();
    
    /**
     * True if has "SignedDocumentList" element
     */
    boolean isSetSignedDocumentList();
    
    /**
     * Sets the "SignedDocumentList" element
     */
    void setSignedDocumentList(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.SignedDocumentListType signedDocumentList);
    
    /**
     * Appends and returns a new empty "SignedDocumentList" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.SignedDocumentListType addNewSignedDocumentList();
    
    /**
     * Unsets the "SignedDocumentList" element
     */
    void unsetSignedDocumentList();
    
    /**
     * Gets the "StateID" element
     */
    java.lang.String getStateID();
    
    /**
     * Gets (as xml) the "StateID" element
     */
    org.apache.xmlbeans.XmlString xgetStateID();
    
    /**
     * True if has "StateID" element
     */
    boolean isSetStateID();
    
    /**
     * Sets the "StateID" element
     */
    void setStateID(java.lang.String stateID);
    
    /**
     * Sets (as xml) the "StateID" element
     */
    void xsetStateID(org.apache.xmlbeans.XmlString stateID);
    
    /**
     * Unsets the "StateID" element
     */
    void unsetStateID();
    
    /**
     * Gets the "GeneratedPolicies" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.GeneratedPoliciesType getGeneratedPolicies();
    
    /**
     * True if has "GeneratedPolicies" element
     */
    boolean isSetGeneratedPolicies();
    
    /**
     * Sets the "GeneratedPolicies" element
     */
    void setGeneratedPolicies(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.GeneratedPoliciesType generatedPolicies);
    
    /**
     * Appends and returns a new empty "GeneratedPolicies" element
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.GeneratedPoliciesType addNewGeneratedPolicies();
    
    /**
     * Unsets the "GeneratedPolicies" element
     */
    void unsetGeneratedPolicies();
    
    /**
     * Gets the "DataType" attribute
     */
    java.lang.String getDataType();
    
    /**
     * Gets (as xml) the "DataType" attribute
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeDataType xgetDataType();
    
    /**
     * Sets the "DataType" attribute
     */
    void setDataType(java.lang.String dataType);
    
    /**
     * Sets (as xml) the "DataType" attribute
     */
    void xsetDataType(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeDataType dataType);
    
    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();
    
    /**
     * Gets (as xml) the "name" attribute
     */
    arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeName xgetName();
    
    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);
    
    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeName name);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType newInstance() {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
