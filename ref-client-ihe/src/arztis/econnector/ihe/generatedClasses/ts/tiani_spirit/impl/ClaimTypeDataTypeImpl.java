/*
 * XML Type:  ClaimType_DataType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeDataType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML ClaimType_DataType(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeDataType.
 */
public class ClaimTypeDataTypeImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeDataType
{
    private static final long serialVersionUID = 1L;
    
    public ClaimTypeDataTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ClaimTypeDataTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
