/*
 * XML Type:  ClaimValue
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML ClaimValue(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue.
 */
public class ClaimValueImpl extends org.apache.xmlbeans.impl.values.XmlAnySimpleTypeImpl implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimValue
{
    private static final long serialVersionUID = 1L;
    
    public ClaimValueImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected ClaimValueImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    
}
