/*
 * XML Type:  IndividualRequestPolicyType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IndividualRequestPolicyType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML IndividualRequestPolicyType(@urn:tiani-spirit:ts).
 *
 * This is a complex type.
 */
public class IndividualRequestPolicyTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IndividualRequestPolicyType
{
    private static final long serialVersionUID = 1L;
    
    public IndividualRequestPolicyTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
