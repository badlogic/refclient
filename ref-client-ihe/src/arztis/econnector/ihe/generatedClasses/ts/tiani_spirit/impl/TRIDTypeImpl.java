/*
 * XML Type:  TRIDType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRIDType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML TRIDType(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRIDType.
 */
public class TRIDTypeImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRIDType
{
    private static final long serialVersionUID = 1L;
    
    public TRIDTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected TRIDTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    
}
