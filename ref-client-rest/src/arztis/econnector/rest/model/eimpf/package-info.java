/**
 * Provides the classes which are needed for the CDA generation and extraction of electronic immunization documents. 
 */
package arztis.econnector.rest.model.eimpf;